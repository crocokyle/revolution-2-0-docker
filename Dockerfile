# syntax=docker/dockerfile:1
FROM registry.gitlab.com/crocokyle/fxserver-base:latest
EXPOSE 30160
EXPOSE 40160
WORKDIR /root/FXServer/server-data
CMD ["bash", "/root/FXServer/server/run.sh", "+set txAdminPort 40160", "server.cfg"]
