# "Revolution" 2.0

### [FXServer-Base Status](https://gitlab.com/crocokyle/fxserver-base)
[![pipeline status](https://gitlab.com/crocokyle/fxserver-base/badges/main/pipeline.svg)](https://gitlab.com/crocokyle/fxserver-base/-/commits/main)

### Build a Windows Test Server

- Clone the [`revolution-2.0`](https://gitlab.com/revolutionlife/revolution-2.0) repo if you haven't already
- Open your `revolution-2.0` folder and place a `server.cfg` file directly in the root.
- Open a command prompt and run the following commands
```batch
cd revolution-2.0
docker run -p 30160:30160 -p 30160:30160/udp -p 40160:40160 -p 40160:40160/udp -ti -v %cd%:/root/FXServer/server-data registry.gitlab.com/crocokyle/revolution-2-0-docker
```

## Build a Linux Test Server
- Clone the [`revolution-2.0`](https://gitlab.com/revolutionlife/revolution-2.0) repo if you haven't already
- Open your `revolution-2.0` folder and place a `server.cfg` file directly in the root.
- Open a terminal and run the following commands
```bash
cd revolution-2.0
docker run -p 30160:30160 -p 30160:30160/udp -p 40160:40160 -p 40160:40160/udp -ti -v $(pwd):/root/FXServer/server-data registry.gitlab.com/crocokyle/revolution-2-0-docker
```

### ToDo when name changes
[ ] Login Screen

### Resourcee Organization
To keep things easy to edit and locate as we continue redevelopment. To keep everything uniform, each resource will will follow the following format:

```
    [Resource Folder]
        L client
            L ...
        L shared
            L ...
        L server
            L ...
        L fxmanifest.lua
        L README.md
```

In the README, include usage, any and all exports and events with usage examples of each

See [CORE]/rp-core

While coding, try to add comments as much as possible to allow for easy location

### Your server.cfg

For easier project mangment and to streamline the coding/definng resources, create a server.cfg in your base server folder along with the git project (Folder should be called revolution-2.0). Have this as your server.cfg, remember to set your license key, and server name. Additionaly you can uncomment sv_master "" to hide your server from the list

```
    # Only change the IP if you're using a server with multiple network interfaces, otherwise change the port only.
    endpoint_add_tcp "0.0.0.0:30120"
    endpoint_add_udp "0.0.0.0:30120"

    # Server Convars

    # Load resources
    exec ./resources/loadresources.cfg


    # This allows players to use scripthook-based plugins such as the legacy Lambda Menu.
    # Set this to 1 to allow scripthook. Do note that this does _not_ guarantee players won't be able to use external plugins.
    sv_scriptHookAllowed 0

    # Uncomment this and set a password to enable RCON. Make sure to change the password - it should look like rcon_password "YOURPASSWORD"
    #rcon_password ""

    # A comma-separated list of tags for your server.
    # For example:
    # - sets tags "drifting, cars, racing"
    # Or:
    # - sets tags "roleplay, military, tanks"
    sets tags "default"

    # A valid locale identifier for your server's primary language.
    # For example "en-US", "fr-CA", "nl-NL", "de-DE", "en-GB", "pt-BR"
    sets locale "root-AQ" 
    # please DO replace root-AQ on the line ABOVE with a real language! :)

    # Set an optional server info and connecting banner image url.
    # Size doesn't matter, any banner sized image will be fine.
    sets banner_detail "https://i.imgur.com/rQlbaaN.png"
    sets banner_connecting "https://i.imgur.com/rQlbaaN.png"

    # Set your server's hostname
    sv_hostname "^4Revolution Life 2.0 ^7|| ^4Dev Server - blm456"

    # Nested configs!
    #exec server_internal.cfg

    # Loading a server icon (96x96 PNG file)
    #load_server_icon myLogo.png

    # convars which can be used in scripts
    set temp_convar "hey world!"

    # Uncomment this line if you do not want your server to be listed in the server browser.
    # Do not edit it if you *do* want your server listed.
    sv_master1 ""

    # Add system admins
    add_ace group.admin command allow # allow all commands
    add_ace group.admin command.quit deny # but don't allow quit
    add_principal identifier.fivem:1 group.admin # add the admin to the group

    # Hide player endpoints in external log output.
    sv_endpointprivacy true

    # enable OneSync with default configuration (required for server-side state awareness)
    onesync_enabled true

    # Server player slot limit (must be between 1 and 32, unless using OneSync)
    sv_maxclients 64

    # Steam Web API key, if you want to use Steam authentication (https://steamcommunity.com/dev/apikey)
    # -> replace "" with the key
    set steam_webApiKey "E7653A3E99EC7E259CCBC1728884C591"

    # License key for your server (https://keymaster.fivem.net)
    sv_licenseKey changeMe
```

Then, create a hard juntion link by opening you command propmt and navigating to the server filles folder

```
mklink /J resources revolution-2.0\resources
```
There should now be a shortcut to the project resources folder
